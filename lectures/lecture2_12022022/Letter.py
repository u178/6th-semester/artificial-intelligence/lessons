from dataclasses import dataclass, field
import numpy as np


@dataclass(order=True)
class Letter:
    img: np.ndarray = field(compare=False)
    bbox: tuple = field(compare=False)
    left_bound: int = field(compare=False)
    right_bound: int = field(compare=False)
    x: int = field(compare=True)
    y: int = field(compare=False)
    name: str = ""
