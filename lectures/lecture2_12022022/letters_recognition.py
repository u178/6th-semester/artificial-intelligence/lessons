from collections import defaultdict
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from cv2 import cv2
from skimage.measure import label, regionprops
from tqdm import tqdm
from sklearn.ensemble import RandomForestClassifier
from Letter import Letter

train_dir = Path('../../data/lecture2/out') / 'train'
train_data = defaultdict(list)
IMG_PATH = '../../data/lecture2/out/5.png'


def combine_i_parts(top: Letter, bottom: Letter, original_img: np.ndarray):
    top_b = top.bbox[0]
    left_b = top.bbox[1] if top.bbox[1] < bottom.bbox[1] else bottom.bbox[1]
    bottom_b = bottom.bbox[2]
    right_b = bottom.bbox[3] if top.bbox[3] < bottom.bbox[3] else top.bbox[3]
    bb = (top_b, left_b, bottom_b, right_b)
    x = (top.x + bottom.x + 1) // 2
    y = (top.y + bottom.y + 1) // 2
    image = original_img[top_b:bottom_b, left_b:right_b]

    return Letter(image, bb, left_b, right_b, x, y, name='i')


def fix_i_separation(words: list, original_img: np.ndarray):
    for i in range(len(words)):
        distance_x = []
        for j in range(len(words[i]) - 1):
            distance_x.append(words[i][j+1].x - words[i][j].x)

        std_x = np.std(distance_x)

        parts_of_i = []
        for j in range(len(words[i]) - 1):
            if 2 * (words[i][j+1].x - words[i][j].x) <= std_x:
                parts_of_i.append(j)

        new_letters = []
        for k in range(len(parts_of_i)):
            if words[i][parts_of_i[k]].y > words[i][parts_of_i[k] +1].y:
                letter_i = combine_i_parts(words[i][parts_of_i[k]+1], words[i][parts_of_i[k]], original_img)
            else:
                letter_i = combine_i_parts(words[i][parts_of_i[k]], words[i][parts_of_i[k] +1], original_img)
            new_letters.append(letter_i)

        # don't do this at home
        for k in reversed(range(len(parts_of_i))):
            index = parts_of_i[k]
            words[i].pop(index+1)
            words[i][index] = new_letters[k]

    return words


def create_letters(bin_img: np.ndarray):
    labeled = label(bin_img)
    regions = regionprops(labeled)
    letters = []
    for i in range(len(regions)):
        bbox = regions[i].bbox
        y = (bbox[2] - bbox[0]) // 2 + bbox[0]
        x = (bbox[3] - bbox[1]) // 2 + bbox[1]
        letters.append(Letter(regions[i].image.astype(int), bbox, bbox[1], bbox[3], x, y))
    return sorted(letters)


def letters2strings(letters):
    distance = []
    for i in range(len(letters) - 1):
        distance.append(letters[i + 1].left_bound - letters[i].right_bound)
    std = np.std(distance)

    splitted_words = [[]]
    pointer = 0
    for i in range(len(distance)):
        # print(i)
        splitted_words[pointer].append(letters[i])
        if distance[i] > 2 * std:
            # print(i)
            pointer += 1
            splitted_words.append([])
    splitted_words[-1].append(letters[-1])
    return splitted_words


def string2textKNN(knn, string):
    answer = []
    for i in range(len(string)):
        answer.append("")
        for j in range(len(string[i])):
            img = string[i][j].img.astype('uint8')
            img_features = np.array(extract_features(img), dtype='f4')

            # TODO change features index
            ret, results, neighbours, dist = knn.findNearest(img_features.reshape(1, 6), 3)
            answer[i] += (chr(int(ret)))
    return ' '.join(answer)


def string2textRF(rf, string):
    answer = []
    for i in range(len(string)):
        answer.append("")
        for j in range(len(string[i])):
            img = string[i][j].img.astype('uint8')
            img_features = np.array(extract_features(img), dtype='f4')
            prediction = rf.predict(img_features.reshape(1, 6))
            # print(prediction)
            answer[i] += (chr(int(prediction)))
    return ' '.join(answer)


def extract_features(img):
    features = []
    # not lakes
    _, hierarchy = cv2.findContours(img, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    ext_cnt = 0
    int_cnt = 0
    for i in range(len(hierarchy[0])):
        if hierarchy[0][i][-1] == -1:
            ext_cnt += 1
        elif hierarchy[0][i][-1] == 0:
            int_cnt += 1

    features.extend([ext_cnt, int_cnt])
    # filling factor
    labeled = label(img)
    region = regionprops(labeled)[0]
    filling_factor = region.filled_area / region.bbox_area
    features.append(filling_factor)
    # centroid weight
    centroid = np.array(region.local_centroid) / np.array(region.image.shape)
    features.extend(centroid)
    features.append(region.eccentricity)


    return features


for path in sorted(train_dir.glob('*')):
    if path.is_dir():
        for img_path in path.glob('*.png'):
            symbol = path.name[-1]
            gray = cv2.imread(str(img_path), 0)
            binary = gray.copy()
            binary[binary > 0] = 1
            train_data[symbol].append(binary)


features_array = []
responses = []
for symbol in tqdm(train_data):
    for img in train_data[symbol]:
        features = extract_features(img)
        features_array.append(features)
        responses.append(ord(symbol))

responses = np.array(responses, dtype='f4')
features_array = np.array(features_array, dtype='f4')
print(f"features_array size: {features_array.shape}")
print(f"responses size: {responses.shape}")

# KNN
knn = cv2.ml.KNearest_create()
knn.train(features_array, cv2.ml.ROW_SAMPLE, responses)

rf = RandomForestClassifier(random_state=42)
rf.fit(features_array, responses)


words = cv2.imread(IMG_PATH, 0)
bin_img = words.copy()
bin_img[bin_img > 0] = 1
plt.imshow(bin_img)
plt.show()

# print(image2text(knn, bin_img))

letters = create_letters(bin_img)
splitted_words = letters2strings(letters)
fixed_words = fix_i_separation(splitted_words, bin_img)

# print(string2textKNN(knn, fixed_words))
print(f"KNN answer: {string2textKNN(knn, fixed_words)}")
print(f"RF answer: {string2textRF(rf, fixed_words)}")